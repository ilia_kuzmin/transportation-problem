import numpy as np


class Constants:
    eps = 1e-13


def are_equal(x: float, y: float):
    return is_zero(x - y)


def is_zero(x: float):
    return abs(x) < Constants.eps


def is_non_negative(x: float):
    return x > 0. or is_zero(x)


def read_parameters(path_to_input):
    suppliers_vector = []
    demands_vector = []
    cost_matrix = []
    result = (suppliers_vector, demands_vector, cost_matrix)

    with open(path_to_input) as file:

        for destination, source in \
                list(zip(result, file.read().split('\n\n'))):

            for elem in source.split('\n'):
                destination.append(list(map(lambda x: float(x), elem.split())))

    return np.array(result[0][0]), np.array(result[1][0]), np.array(result[2])


def calculate_optimal_value(tableau, costs):
    opt = 0.
    for i in range(tableau.shape[0]):
        for j in range(tableau.shape[1]):
            if not np.isnan(tableau[i][j]):
                opt += costs[i][j] * tableau[i][j]
    return opt


def is_equal_assert(left_operand, right_operand, message):
    if not is_zero(left_operand - right_operand):
        raise AssertionError(message)


def get_closed_form(supplies, demands, costs):
    delta = sum(supplies) - sum(demands)

    if not is_zero(delta):
        if delta > 0.:
            demands, costs = add_extra_demand(delta, demands, costs)
        else:
            supplies, costs = add_extra_supplier(-delta, supplies, costs)
    return supplies, demands, costs


def add_extra_supplier(delta, supplies, costs):
    assert len(costs.shape) == 2

    return np.append(supplies, delta), \
        np.vstack((costs, np.zeros((1, costs.shape[1]))))


def add_extra_demand(delta, demands, costs):
    assert len(costs.shape) == 2

    return np.append(demands, delta), \
        np.hstack((costs, np.zeros((costs.shape[0], 1))))
