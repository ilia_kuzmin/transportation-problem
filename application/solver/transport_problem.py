import numpy as np
from ..utils.utils import are_equal, is_non_negative, \
    read_parameters, calculate_optimal_value
from collections import deque, namedtuple
from enum import Enum


# finds initial transportation plan
# arguments: supplies - vector of available goods in warehouses
#            demands  - vector of required goods in delivery points
# returns:   tableau  - matrix of initial transportation plan
def northwest_corner_method(supplies, demands):
    assert len(supplies.shape) == 1
    assert len(demands.shape) == 1

    a_v, b_v = np.copy(supplies), np.copy(demands)
    m, n = a_v.shape[0], b_v.shape[0]
    tableau = np.full((m, n), np.nan)
    x_pos, y_pos = 0, 0
    while x_pos < n and y_pos < m:
        if are_equal(a_v[y_pos], b_v[x_pos]):
            tableau[y_pos][x_pos] = a_v[y_pos]
            if x_pos + 1 < n:
                tableau[y_pos][x_pos + 1] = 0.
            x_pos += 1
            y_pos += 1
        elif b_v[x_pos] < a_v[y_pos]:
            tableau[y_pos][x_pos] = b_v[x_pos]
            a_v[y_pos] -= b_v[x_pos]
            x_pos += 1
        elif a_v[y_pos] < b_v[x_pos]:
            tableau[y_pos][x_pos] = a_v[y_pos]
            b_v[x_pos] -= a_v[y_pos]
            y_pos += 1

    return tableau


class VarType(Enum):
    ROW = 1
    COL = 2


def other_var_type(var_type):
    return VarType.ROW if var_type is VarType.COL else VarType.COL


def append_undef_var_ind(tableau, ind, var_type, var, queue):
    vec = tableau[ind, :] if var_type is VarType.ROW else tableau[:, ind]
    for i in range(len(vec)):
        if not np.isnan(vec[i]) and np.isnan(var[i]):
            queue.append((other_var_type(var_type), i, ind))


# calculates current dual variables
# arguments: tableau - matrix of current transportation plan
#            costs   - matrix of costs
# returns:   u, v    - vectors of current dual variables
def get_potentials(tableau, costs):
    assert len(tableau.shape) == 2

    m, n = tableau.shape
    u, v = np.full(m, np.nan), np.full(n, np.nan)
    u[0] = 0.

    queue = deque()
    append_undef_var_ind(tableau, 0, VarType.ROW, v, queue)
    while queue:
        var_type, to_find_ind, found_ind = queue.popleft()
        if var_type is VarType.COL:
            v[to_find_ind] = costs[found_ind][to_find_ind] + u[found_ind]
            append_undef_var_ind(tableau, to_find_ind, var_type, u, queue)
        elif var_type is VarType.ROW:
            u[to_find_ind] = v[found_ind] - costs[to_find_ind][found_ind]
            append_undef_var_ind(tableau, to_find_ind, var_type, v, queue)
    return u, v


# checks current plan to see if it's optimal
# arguments: tableau          - matrix of current transportation plan
#            costs            - matrix of costs
# returns:   boolean variable - whether or not the current plan is optimal
#            new cell         - indices of cell where optimal criteria is
#                                                                violated
def is_optimal(tableau, costs):
    u, v = get_potentials(tableau, costs)
    m, n = tableau.shape

    new_cell = (-1, -1)
    for i in range(m):
        for j in range(n):
            if not is_non_negative(costs[i][j] - v[j] + u[i]):
                new_cell = (i, j)
                break
        else:
            continue
        break
    else:
        return True, new_cell
    return False, new_cell


def get_col_neighbors(cur_y, cur_x, col):
    return [(ind, cur_x) for ind in range(len(col)) if not ind == cur_y and
            not np.isnan(col[ind])]


def get_row_neighbors(cur_y, cur_x, row):
    return [(cur_y, ind) for ind in range(len(row)) if not ind == cur_x and
            not np.isnan(row[ind])]


def get_neighbors(cur_cell, var_type, tableau):
    assert len(tableau.shape) == 2

    cur_y, cur_x = cur_cell
    if var_type == VarType.COL:
        return get_col_neighbors(cur_y, cur_x, tableau[:, cur_x])
    return get_row_neighbors(cur_y, cur_x, tableau[cur_y, :])


def get_cycle_by_parents(new_cell, parents):
    cycle = [new_cell]
    cur_cell = parents[new_cell].parent
    while not cur_cell == new_cell:
        cycle.append(cur_cell)
        cur_cell = parents[cur_cell].parent
    return cycle


# finds recalculation cycle
# arguments: new_cell - tuple with indices of cell where optimal criteria
#                                                             is violated
#            tableau  - matrix of current transportation plan
# returns:   cycle    - vector that stores recalculation cycle
def get_cycle(new_cell, tableau):
    tableau[new_cell[0]][new_cell[1]] = 0.
    stack = deque()
    stack.append(new_cell)

    TableauCell = namedtuple('TableauCell', ['parent', 'type'])
    parents = {new_cell: TableauCell((-1, -1), VarType.COL)}
    while stack:
        cur_cell = stack.pop()
        neighbors = get_neighbors(cur_cell, parents[cur_cell].type, tableau)

        for neighbor in neighbors:
            parents[neighbor] = TableauCell(cur_cell, other_var_type(
                parents[cur_cell].type))
            if neighbor == new_cell:
                parents[new_cell] = TableauCell(cur_cell,
                                                parents[new_cell].type)
                break
            stack.append(neighbor)
        else:
            continue
        break

    return get_cycle_by_parents(new_cell, parents)


def get_min_cell(cycle, tableau):
    cur_min, min_cell, state = float("Inf"), (-1, -1), True
    for y, x in cycle:
        if not state:
            if tableau[y][x] < cur_min:
                cur_min, min_cell = tableau[y][x], (y, x)
        state = not state
    return min_cell


# recalculates current plan with recalculation cycle
# arguments: cycle      - vector with recalculation cycle
#            tableau    - matrix of current transportation plan
#            min_values - vector that stores coordinates of tableau's
#                                            cells with minimal value
def recalculate_tableau(cycle, tableau, min_values):
    state = True
    y_min, x_min = get_min_cell(cycle, tableau)
    min_values.append((y_min, x_min))
    delta = tableau[y_min][x_min]
    for y, x in cycle:
        if state:
            tableau[y][x] += delta
        else:
            tableau[y][x] -= delta
        state = not state
    tableau[y_min][x_min] = np.nan


# solves transportation problem
# arguments: supplies   - vector of available goods in warehouses
#            demands    - vector of required goods in delivery points
#            costs      - matrix of costs
# returns:   tableau    - optimal plan
#            iters      - number of iterations to get the solution
#            tableaux   - array of matrices with all transportation plans
#            cycles     - array of vectors with all recalculation cycles
#            min_values - vector with minimal values of each transportation
#                                                                      plan
def potential_method(supplies, demands, costs):
    tableaux = []
    cycles = []
    min_values = []
    tableau = northwest_corner_method(supplies, demands)

    tableaux.append(np.copy(tableau))
    iters = 0
    while True:
        res, new_cell = is_optimal(tableau, costs)
        if res:
            cycles.append(None)
            return tableau, iters, tableaux, cycles, min_values
        cycle = get_cycle(new_cell, tableau)
        recalculate_tableau(cycle, tableau, min_values)
        tableaux.append(np.copy(tableau))
        cycles.append(cycle.copy())
        iters += 1


def get_optimal_value(path_to_file):
    suppliers_vector, demands_vector, cost_matrix = \
        read_parameters(path_to_file)
    tableau, _, _, _, _ = \
        potential_method(suppliers_vector, demands_vector, cost_matrix)

    return calculate_optimal_value(tableau, cost_matrix)
