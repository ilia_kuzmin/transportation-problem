import numpy as np
from PyQt5 import QtCore, QtGui, QtWidgets


class UserInterfaceWindow(object):

    def __init__(self):
        self.grid_layout = None
        self.table_widget = None
        self.back_button = None
        self.pages_bar = None
        self.next_button = None
        self.optimal_label = None

    @staticmethod
    def window_initialization(window):
        window.setObjectName("Window")
        window.resize(1100, 900)
        window.setSizeGripEnabled(False)

        return window

    def set_grid_layout(self, window):
        self.grid_layout = QtWidgets.QGridLayout(window)
        self.grid_layout.setObjectName("grid_layout")
        spacer_item = \
            QtWidgets.QSpacerItem(20, 879,
                                  QtWidgets.QSizePolicy.Minimum,
                                  QtWidgets.QSizePolicy.Expanding)
        self.grid_layout.addItem(spacer_item, 0, 0, 3, 1)
        self.grid_layout.addWidget(self.table_widget, 0, 1, 1, 5)
        spacer_item = \
            QtWidgets.QSpacerItem(20, 879,
                                  QtWidgets.QSizePolicy.Minimum,
                                  QtWidgets.QSizePolicy.Expanding)
        self.grid_layout.addItem(spacer_item, 0, 6, 3, 1)
        self.grid_layout.addWidget(self.back_button, 2, 1, 1, 1)
        spacer_item = \
            QtWidgets.QSpacerItem(277, 20,
                                  QtWidgets.QSizePolicy.Expanding,
                                  QtWidgets.QSizePolicy.Minimum)
        self.grid_layout.addItem(spacer_item, 2, 2, 1, 1)
        self.grid_layout.addWidget(self.pages_bar, 2, 3, 1, 1)
        spacer_item = \
            QtWidgets.QSpacerItem(277, 20,
                                  QtWidgets.QSizePolicy.Expanding,
                                  QtWidgets.QSizePolicy.Minimum)
        self.grid_layout.addItem(spacer_item, 2, 4, 1, 1)
        self.grid_layout.addWidget(self.next_button, 2, 5, 1, 1)
        self.grid_layout.addWidget(self.optimal_label, 1, 3, 1, 1)

    def set_table_widget(self, window):
        self.table_widget = TransportTableWidget(window)
        size_policy = \
            QtWidgets.QSizePolicy(
                QtWidgets.QSizePolicy.Expanding,
                QtWidgets.QSizePolicy.Expanding)
        size_policy.setHorizontalStretch(0)
        size_policy.setVerticalStretch(0)
        size_policy.setHeightForWidth(
            self.table_widget.sizePolicy().hasHeightForWidth())
        self.table_widget.setSizePolicy(size_policy)
        self.table_widget.setFrameShape(QtWidgets.QFrame.WinPanel)
        self.table_widget.setRowCount(4)
        self.table_widget.setColumnCount(4)
        self.table_widget.setObjectName("table_widget")
        self.table_widget.horizontalHeader().setCascadingSectionResizes(False)
        self.table_widget.horizontalHeader().setSortIndicatorShown(False)
        self.table_widget.verticalHeader().setCascadingSectionResizes(False)
        self.table_widget.verticalHeader().setStretchLastSection(False)

    def set_back_button(self, window, font):
        self.back_button = QtWidgets.QPushButton(window)
        self.back_button.setFont(font)
        self.back_button.setFocusPolicy(QtCore.Qt.NoFocus)
        self.back_button.setAutoDefault(False)
        self.back_button.setObjectName("back_button")

    def set_pages_bar(self, window, font):
        self.pages_bar = QtWidgets.QProgressBar(window)
        self.pages_bar.setFont(font)
        self.pages_bar.setProperty("value", 24)
        self.pages_bar.setObjectName("pages_bar")

    def set_next_button(self, window, font):
        self.next_button = QtWidgets.QPushButton(window)
        self.next_button.setFont(font)
        self.next_button.setFocusPolicy(QtCore.Qt.NoFocus)
        self.next_button.setAutoDefault(False)
        self.next_button.setDefault(False)
        self.next_button.setFlat(False)
        self.next_button.setObjectName("next_button")

    def set_optimal_label(self, window, font):
        self.optimal_label = QtWidgets.QLabel(window)
        self.optimal_label.setText("")
        self.optimal_label.setObjectName("optimal_label")
        self.optimal_label.setAlignment(QtCore.Qt.AlignCenter)
        self.optimal_label.setFont(font)

    @staticmethod
    def tibetan_machine_font():
        font = QtGui.QFont()
        font.setFamily("Tibetan Machine Uni")
        font.setPointSize(14)
        font.setBold(True)
        font.setWeight(75)
        return font

    @staticmethod
    def dejavu_sans_font():
        font = QtGui.QFont()
        font.setFamily("DejaVu Sans")
        font.setPointSize(13)
        font.setBold(True)
        font.setItalic(False)
        font.setWeight(75)
        return font

    def setup_user_interface(self, window):
        tibetan_font = self.tibetan_machine_font()
        dejavu_font = self.dejavu_sans_font()
        window = self.window_initialization(window)
        self.set_table_widget(window)
        self.set_back_button(window, tibetan_font)
        self.set_pages_bar(window, dejavu_font)
        self.set_next_button(window, tibetan_font)
        self.set_optimal_label(window, tibetan_font)
        self.set_grid_layout(window)
        self.retranslate_user_interface(window)
        QtCore.QMetaObject.connectSlotsByName(window)

    def retranslate_user_interface(self, window):
        translate = QtCore.QCoreApplication.translate
        window.setWindowTitle(translate("Window", "Transportation problem"))
        sortingEnabled = self.table_widget.isSortingEnabled()
        self.table_widget.setSortingEnabled(False)
        self.table_widget.setSortingEnabled(sortingEnabled)
        self.back_button.setText(translate("Window", "BACK"))
        self.pages_bar.setFormat(translate("Window", "%v/%m"))
        self.next_button.setText(translate("Window", "NEXT"))


class TransportTableWidget(QtWidgets.QTableWidget):

    def __init__(self, parent):
        QtWidgets.QTableWidget.__init__(self, parent)
        self.paint_cycle = False

    def draw_horizontal_line(self, painter, col_start,
                             col_end, row):
        x_start = \
            self.columnViewportPosition(col_start) + \
            self.columnWidth(col_start) // 2
        x_end = \
            self.columnViewportPosition(col_end) + \
            self.columnWidth(col_end) // 2
        y = \
            self.rowViewportPosition(row) + \
            self.rowHeight(row) // 2
        painter.drawLine(x_start, y, x_end, y)

    def draw_vertical_line(self, painter, row_start, row_end, col):
        y_start = self.rowViewportPosition(row_start) + \
                  self.rowHeight(row_start) // 2
        y_end = \
            self.rowViewportPosition(row_end) + \
            self.rowHeight(row_end) // 2
        x = self.columnViewportPosition(col) + \
            self.columnWidth(col) // 2
        painter.drawLine(x, y_start, x, y_end)

    def draw_cycle(self):
        painter = QtGui.QPainter(self.viewport())

        cycle = self.cycles[self.cur_page]
        shifted = cycle[1:] + cycle[:1]
        for start, end in zip(cycle, shifted):
            if start[0] == end[0]:
                self.draw_horizontal_line(painter, start[1], end[1], start[0])
            elif start[1] == end[1]:
                self.draw_vertical_line(painter, start[0], end[0], start[1])

    def paintEvent(self, event):
        if self.paint_cycle and not self.cycles[self.cur_page] is None:
            self.set_table(True)
            self.draw_cycle()
        else:
            self.set_table(False)
        QtWidgets.QTableWidget.paintEvent(self, event)

    def keyPressEvent(self, QKeyEvent):
        if QKeyEvent.key() == QtCore.Qt.Key_Space:
            self.paint_cycle = not self.paint_cycle
            self.viewport().repaint()

    def set_table(self, show_cycle=False):
        for i in range(self.rowCount()):
            for j in range(self.columnCount()):
                item = get_standard_item(
                    get_data(self.costs[i][j],
                             self.tableaux[self.cur_page][i][j]))
                self.setItem(i, j, item)

        state = True
        if show_cycle:
            for i, j in self.cycles[self.cur_page]:
                y_min, x_min = self.min_values[self.cur_page]
                if state:
                    item = get_green_item(get_data_for_cycle(
                        self.costs[i][j], self.tableaux[self.cur_page][i][j],
                        self.tableaux[self.cur_page][y_min][x_min], True))
                    self.setItem(i, j, item)
                    state = False
                else:
                    item = get_red_item(get_data_for_cycle(
                        self.costs[i][j], self.tableaux[self.cur_page][i][j],
                        self.tableaux[self.cur_page][y_min][x_min], False))
                    self.setItem(i, j, item)
                    state = True


def get_data(cost, elem):
    return str(cost) + ":\n\n" + ("" if np.isnan(elem) else str(elem))


def get_data_for_cycle(cost, elem, extra, plus):
    return get_data(cost, elem) + \
           " (" + \
           ("+" if plus else "-") + str(extra) + ")"


def get_empty_item():
    item = QtWidgets.QTableWidgetItem()
    item.setFlags(QtCore.Qt.NoItemFlags)

    return item


def get_standard_item(data):
    item = QtWidgets.QTableWidgetItem(str(data))
    item.setTextAlignment(QtCore.Qt.AlignCenter)
    font = QtGui.QFont()
    font.setPointSize(15)
    font.setBold(True)
    item.setFont(font)
    item.setFlags(QtCore.Qt.NoItemFlags)

    brush = QtGui.QBrush(QtGui.QColor(0, 0, 0, 255))
    brush.setStyle(QtCore.Qt.NoBrush)
    item.setForeground(brush)

    return item


def get_green_item(data):
    item = get_standard_item(data)
    brush = QtGui.QBrush(QtGui.QColor(0, 100, 0, 100))
    brush.setStyle(QtCore.Qt.SolidPattern)
    item.setBackground(brush)
    item.setFlags(QtCore.Qt.NoItemFlags)

    return item


def get_red_item(data):
    item = get_standard_item(data)
    brush = QtGui.QBrush(QtGui.QColor(150, 0, 0, 100))
    brush.setStyle(QtCore.Qt.SolidPattern)
    item.setBackground(brush)
    item.setFlags(QtCore.Qt.NoItemFlags)

    return item
