from PyQt5.QtWidgets import QDialog
from .base_interface import UserInterfaceWindow


class TableauWindow(UserInterfaceWindow, QDialog):
    def __init__(self, tableaux, costs, supplies,
                 demands, cycles, optimal, min_values):
        QDialog.__init__(self)
        self.setup_user_interface(self)
        self.table_widget.horizontalHeader().setSectionResizeMode(True)
        self.table_widget.verticalHeader().setSectionResizeMode(True)

        self.pages_bar.setMaximum(len(tableaux))
        self.pages_bar.setValue(1)

        self.table_widget.tableaux = tableaux
        self.table_widget.cycles = cycles
        self.optimal = optimal
        self.table_widget.min_values = min_values
        self.table_widget.costs = costs

        self.table_widget.cur_page = 0
        self.table_widget.setColumnCount(tableaux[0].shape[1])
        self.table_widget.setRowCount(tableaux[0].shape[0])
        self.table_widget.setHorizontalHeaderLabels(
            list(map(lambda v: "B  " + str(v), demands)))
        self.table_widget.setVerticalHeaderLabels(
            list(map(lambda v: "A  " + str(v), supplies)))
        self.table_widget.set_table()

        self.back_button.clicked.connect(self.back_button_clicked)
        self.next_button.clicked.connect(self.next_button_clicked)

        if len(tableaux) == 1:
            self.optimal_label.setText("Optimal expenses are " +
                                       str(self.optimal))

    def back_button_clicked(self):
        if self.pages_bar.value() == 1:
            return
        self.pages_bar.setValue(self.pages_bar.value() - 1)
        self.table_widget.paint_cycle = False
        self.table_widget.cur_page = self.pages_bar.value() - 1
        self.table_widget.set_table()
        self.optimal_label.setText("")

    def next_button_clicked(self):
        if self.pages_bar.value() == self.pages_bar.maximum():
            return
        self.pages_bar.setValue(self.pages_bar.value() + 1)
        self.table_widget.paint_cycle = False
        self.table_widget.cur_page = self.pages_bar.value() - 1
        self.table_widget.set_table()

        if self.pages_bar.value() == self.pages_bar.maximum():
            self.optimal_label.setText("Optimal expenses are " +
                                       str(self.optimal))
