import sys
import argparse
import numpy as np

from application.solver.transport_problem import potential_method
from application.graphics.solution_window import TableauWindow
from application.utils.utils import read_parameters, calculate_optimal_value, \
    get_closed_form
from PyQt5.QtWidgets import QApplication


def str_to_bool(arg):
    if arg.lower() in ['true', 't']:
        return True
    elif arg.lower() in ['false', 'f']:
        return False
    else:
        raise argparse.ArgumentTypeError('Incorrect argument')


def create_parser():
    arg_parser = argparse.ArgumentParser(add_help=True)
    arg_parser.add_argument('--input', nargs='?', type=str,
                            help='path to file containing problem parameters',
                            default='tests/inputs/input0')
    arg_parser.add_argument('--output', nargs='?', type=str,
                            help='path to file where '
                                 'results should be written',
                            default='output.txt')
    arg_parser.add_argument('--show', nargs='?', type=str_to_bool,
                            help='show solution - True(t)/False(f)',
                            default='True')
    return arg_parser


def print_results(path_to_output, tableau, optimal_value, iterations):
    with open(path_to_output, 'w') as file:
        print('Optimal expenses are ' + str(optimal_value), file=file)
        print('Number of iterations is ' + str(iterations), file=file)
        print('Optimal solution is:', file=file)

        for i in range(tableau.shape[0]):
            for j in range(tableau.shape[1]):
                to_print = 0. if np.isnan(tableau[i][j]) else tableau[i][j]
                print(to_print, end=' ', file=file)
            print('', file=file)


def main():
    parser = create_parser()
    namespace = parser.parse_args(sys.argv[1:])
    sup, dem, costs = read_parameters(namespace.input)

    sup, dem, costs = get_closed_form(sup, dem, costs)
    tableau, iterations, tableaux, cycles, min_values \
        = potential_method(sup, dem, costs)

    optimal_value = calculate_optimal_value(tableau, costs)
    print_results(namespace.output, tableau, optimal_value, iterations)

    if namespace.show:
        app = QApplication(sys.argv)
        window = TableauWindow(tableaux, costs, sup, dem, cycles,
                               optimal_value, min_values)
        window.show()
        sys.exit(app.exec())


if __name__ == '__main__':
    main()
