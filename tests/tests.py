import os
import json
import unittest

from application.utils.utils import is_equal_assert
from application.solver.transport_problem import get_optimal_value


class Tests(unittest.TestCase):

    def setUp(self):
        config_path = os.environ.get('PATH_TO_CONFIG') or './tests/config'

        with open(config_path) as f:
            self.config = json.load(f)

    def test_regular_data(self):

        for test in self.config['regular_data']:
            is_equal_assert(float(test['result']),
                            get_optimal_value(self.config["path_to_input"] +
                                              test['path_suffix']),
                            "failed regular data test number - "
                            f"{test['path_suffix']}")

    def test_big_data(self):

        for test in self.config['big_data']:
            is_equal_assert(float(test['result']),
                            get_optimal_value(self.config["path_to_input"] +
                                              test['path_suffix']),
                            "failed big data test number - "
                            f"{test['path_suffix']}")

    def test_fictitious_seller(self):

        for test in self.config['fictitious_seller']:
            is_equal_assert(float(test['result']),
                            get_optimal_value(self.config["path_to_input"] +
                                              test['path_suffix']),
                            "failed fictitious seller test number - "
                            f"{test['path_suffix']}")

    def test_edge_cases(self):

        for test in self.config['edge_cases']:
            is_equal_assert(float(test['result']),
                            get_optimal_value(self.config["path_to_input"] +
                                              test['path_suffix']),
                            "failed edge cases test number - "
                            f"{test['path_suffix']}")


if __name__ == "__main__":
    unittest.main()
