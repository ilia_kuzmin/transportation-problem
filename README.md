# About 
The application solves transportation problem using potential method  
and demonstrates the solution by steps on the screen

# Requirements 
+ PyQt5  
+ numpy
# How to install
1. Clone repository
2. Go to the cloned directory  
3. Create virtual environment:  
   + For UNIX:
        + python3 -m venv env
        + source env/bin/activate 
    + For Windows:
        + python3 -m venv env
4. Download requirements:  
    pip3 install -r requirements     
# How to use
+ To start application run such command in your terminal:  
python3 main.py  
+ To specify input file run:  
python3 main.py --input tests/inputs/inputN.txt  
where N is a number from 0 to 7   
+ To see all command line arguments run:  
python3 main.py --help   
+ While running the application you can enable/disable   
recalculation cycle by pressing ___space bar___   